import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import './plugins/index'
import mitt from 'mitt';

/* Integración de componentes de Quasar framework */
import { Quasar } from 'quasar'
import quasarUserOptions from './quasar-user-options'

const app = createApp(App).use(Quasar, quasarUserOptions)
  .use(store)
  .use(router)
  .provide('bus', mitt());
  
router.isReady().then(() => {
  app.mount('#app');
});