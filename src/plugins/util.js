// Archivo de utilidades globales para la aplicación
/*************************************************************/
// 
// @author Felix Mamani (felixddxd@gmail.com)
// @entity Pragma Invest S.A.
//
/*************************************************************/
'use strict'
import store from '../store/index'
import router from '../router/index'
import { useQuasar } from 'quasar';

const util = {
  // Format number money
  numberMoney (number) {
    var formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      // These options are needed to round to whole numbers if that's what you want.
      //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
      //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    let result = formatter.format(number).split('$');
    return result[1];
  },

  // Verify rol user
  verifyRol (roles) {
    // ***** Roles del sistema *****
    // 1: ADMINISTRADOR
    // 2: USUARIO
    // 3: AUTORIZADOR
    // 4: REVISOR
    // 5: APROBADOR
    // 6: TESORERIA
    // 7: APROBADOR RENDICIONES
    // 8: ADMIN
    const user = store.getters['auth/user']
    let sw = false
    roles.forEach(item => {
      if (item == user.rol_id) {
        sw = true
      }
    })
    if (!sw) {
      router.push({name: '403'})
    }
  },

  // Verify if exist Business on application
  verifyBusiness () {
    const quasar = useQuasar()
    const empresa = quasar.localStorage.getItem('empresa')
    if (!empresa) {
      return false
    } else {
      return true
    }
  }
}

export default util;