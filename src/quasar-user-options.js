
import 'quasar/dist/quasar.css'
import lang from 'quasar/lang/es.js'
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'
import './css/app.css'
import {Notify, Loading, LocalStorage} from 'quasar'

// To be used on app.use(Quasar, { ... })
export default {
  config: {
    notify: {
      timeout: 1000,
      position: 'bottom'
    },
    loading: {
      message: 'Cargando... Espere por favor'
    },
    brand: {
      pragma_pri: '#a53042'
    }
  },
  plugins: {
    Notify,
    Loading,
    LocalStorage
  },
  lang: lang
}