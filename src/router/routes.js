export default [
  ...applyRules(['guest'], [
    {
      path: '/',
      component: () => import('../layouts/AuthLayout.vue'),
      children: [
        {
          path: '',
          name: 'login',
          component: () => import('../views/auth/Index.vue')
        },
        {
          path: '/recovery_password/:code',
          name: 'recovery_password',
          component: () => import('../views/auth/RecoveryPassword.vue')
        }
      ]
    }
  ]),
  ...applyRules(['auth'], [
    {
      path: '/choose-business',
      component: () => import('../layouts/WrapperLayout.vue'),
      children: [
        {
          path: '',
          name: 'choose-business',
          component: () => import('../views/dashboard/ChooseBusiness.vue')
        }
      ]
    },
    {
      path: '/solicitudes',
      component: () => import('../layouts/SolicitudLayout.vue'),
      children: [
        // Pantalla inicial Dashboard
        {
          path: '',
          name: 'indexSolicitud',
          component: () => import('../views/solicitud/Solicitud.vue')
        },
        {
          path: 'search',
          name: 'searchSolicitud',
          component: () => import('../views/solicitud/BuscarSolicitud.vue')
        },
      ]
    },
    {
      path: '/autorizaciones',
      component: () => import('../layouts/AutorizadorLayout.vue'),
      children: [
        // Pantalla inicial Dashboard
        {
          path: '',
          name: 'indexAutorizador',
          component: () => import('../views/autorizador/Autorizador.vue')
        },
        {
          path: 'searchAutorizar',
          name: 'searchAutorizar',
          component: () => import('../views/autorizador/BuscarAutorizar.vue')
        },
      ]
    },
    {
      path: '/403',
      name: '403',
      component:() => import('../views/common/403.vue')
    }
  ]),
  {
    path: '/:catchAll(.*)',
    redirect: { name: 'indexSolicitud' } 
  }
]

function applyRules(rules, routes) {
  for (let i in routes) {
    routes[i].meta = routes[i].meta || {}

    if (!routes[i].meta.rules) {
      routes[i].meta.rules = []
    }
    routes[i].meta.rules.unshift(...rules)

    if (routes[i].children) {
      routes[i].children = applyRules(rules, routes[i].children)
    }
  }

  return routes
}